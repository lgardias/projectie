<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2017-01-17
 * Time: 19:54
 */

namespace AppBundle\Controller;



use AppBundle\AppBundle;
use AppBundle\Entity\Card;
use AppBundle\Entity\Deck;
use AppBundle\Entity\Comment;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GaleryController extends Controller{

    /**
     * @Route("/decks", name="decks")
     */
    public function showDecks(){
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Deck');
        $entities = $repository->findAll();


        return $this->render('default/deck.html.twig', array(
            'decks' => $entities
        ));
    }

    /**
     *@Route("/deck/{id}", name="deck_viev")
     */
    public function showDeck(Deck $deck, Request $request){

        $form = null;
        iF($user = $this -> getUser()){
            $comment = new Comment();
            $comment -> setDeck($deck);
            $comment ->setUser($user);

            $form = $this->createFormBuilder($comment)
                ->add('content', TextType::class, array(
                    'label' => false,
                    'attr' => array('placeholder' => "Treść komentarza")
                ))
            ->getForm();

            $form->handleRequest($request);

            if($form->isValid()){

                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                $this->addFlash('success', 'Komentarz został pomyślnie dodany');

                return $this->redirectToRoute('deck_viev', array('id' => $deck->getId()));

            }

        }

        return $this->render('default/showDeck.html.twig', array(
            'deck' => $deck,
            'form' => is_null($form) ? $form : $form->createView()
        ));

    }
    /**
     * @Route("/galery", name="galery")
     */
    public function showGalery(){

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Card');
            $entities = $repository->findAll();


        return $this->render('default/galery.html.twig', array(
            'cards' => $entities
        ));
    }

    /**
     * @Route("/galerySortBy/{Class}", name="SortGalery")
     */

    public function showSortGalery(Card $card){

        $class = $card.get_class();
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('AppBundle:Card');
        $entities = $repository->findByClass($class);


        return $this->render('default/galery.html.twig', array(
            'cards' => $entities
        ));

    }

    /**
     *@Route("/card/{id}", name="card_viev")
     */

    public function showCard(Card $card){

        return $this->render('default/card.html.twig', array(
            'card' => $card
        ));

    }

    /**
 * @Route("/OP/{id}", name="OP")
 */
    public function opinionPositive(Deck $deck){

        iF($user = $this -> getUser()){
            $latest = $deck->getPositive();
            $deck ->setPositive($latest + 1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($deck);
            $em->flush();

            return $this->redirectToRoute('deck_viev', array('id' => $deck->getId()));
        }

    }

    /**
     * @Route("/ON/{id}", name="ON")
     */
    public function opinionNegative(Deck $deck){

        iF($user = $this -> getUser()){
            $latest = $deck->getNegative();
            $deck ->setNegative($latest + 1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($deck);
            $em->flush();

            return $this->redirectToRoute('deck_viev', array('id' => $deck->getId()));
        }

    }

}