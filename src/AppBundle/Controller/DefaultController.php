<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Card;
use AppBundle\Entity\Post;
use AppBundle\Entity\Comment;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function showHome(){
        return $this->render('default/home.html.twig', array());
    }
    /**
     * @Route("/blog", name="blog")
     */
    public function indexAction(Request $request)
    {

        $qb = $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->from('AppBundle:Post', 'p')
            ->select('p');

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $request->query->get('page', 1),
            20
        );

        return $this->render('default/index.html.twig', array(
            'posts' => $pagination
        ));
    }

    /**
     * @Route("/arcticle/{id}", name="post_show")
     */
    public function showAction(Post $post, Request $request){

        $form = null;
        if($user = $this->getUser()){
            $comment = new Comment();
            $comment -> setPost($post);
            $comment->setUser($user);

            $form = $this->createFormBuilder($comment)
                ->add('content', TextType::class, array(
                    'label' => false,
                    'attr' => array('placeholder' => "Treść komentarza")
                ))
                ->getForm();

            $form->handleRequest($request);

            if($form->isValid()){

                $em = $this->getDoctrine()->getManager();
                $em->persist($comment);
                $em->flush();

                $this->addFlash('success', 'Komentarz został pomyślnie dodany');

                return $this->redirectToRoute('post_show', array('id' => $post->getId()));

            }
        }


        return $this->render('default/show.html.twig', array(
            'post' => $post,
            'form' => is_null($form) ? $form : $form->createView()
        ));
    }

}
